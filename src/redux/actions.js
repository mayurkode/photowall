import { database } from '../config';

export const startAddingPost = (post) => {
    return (dispatch) => {
        return database.ref('posts').update({ [post.id]: post }).then(() => {
            dispatch(addPhoto(post));
        }).catch((error) => {
            console.error(error);
        });
    }
}

export const startLoadingPosts = () => {
    return (dispatch) => {
        return database.ref('posts').once('value').then((snapshot) => {
            let posts = [];
            snapshot.forEach((snapshotChild) => {
                posts.push(snapshotChild.val());
            });

            dispatch(loadPosts(posts));

        }).catch((error) => {
            console.error(error);
        })
    }
}

export const startRemovingPhoto = (id) => {
   
    return (dispatch) => {
        return database.ref(`posts/${id}`).remove().then(() => {
            dispatch(removePhoto(id));
        }).catch((error) => {
            console.error(error);
        });
    }
}

export const startRemovingPhotoAndComments = (id) => {
    const updates = {
        ['posts/' + id]: null,
        ['comments/' + id]: null
    }

    return (dispatch) => {
        return database.ref().update(updates).then(() => {
            dispatch(removePhoto(id));
        }).catch((error) => {
            console.error(error);
        });
    }
}


export const startAddingComment = (comment, postId) => {
    return (dispatch) => {
        return database.ref('comments/' + postId).push(comment).then(() => {
            dispatch(addComment(comment, postId));
        }).catch((error) => {
            console.error(error);
        });
    }
}

export const startLoadingComments = () => {
    return (dispatch) => {
        return database.ref('comments').once('value').then((snapshot) => {
            let comments = {};
            snapshot.forEach((childSnapshot) => {
                comments[childSnapshot.key] = Object.values(childSnapshot.val());
            });
            console.log(comments);
            dispatch(loadComments(comments));
        })
    }
}


export const startRemovingComments = (id) => {
    return (dispatch) => {
        return database.ref('comments/' + id).remove().then(() => {
            console.log('comment removed too...')
        }).catch((error) => {
            console.log('error');
        })
    }
}

export const loadComments = (comments) => {
    return {
        type: 'LOAD_COMMENTS',
        comments: comments
    }
}

export const loadPosts = (posts) => {
    return {
        type: 'LOAD_POSTS',
        posts: posts
    }
}

export const removePhoto = (id) => {
    return {
        type: 'REMOVE_PHOTO',
        id: id
    }
}

export const addPhoto = (newPhoto) => {
    return {
        type: 'ADD_PHOTO',
        photo: newPhoto
    }
}

export const addComment = (msg, ID) => {
    return {
        type: 'ADD_COMMENT',
        comment: msg,
        postID: ID
    }
}