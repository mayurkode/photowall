
import POSTS from '../data/mock-posts';
import { combineReducers } from 'redux';


const comments = (state = {}, action) => {

    switch (action.type) {
        case 'ADD_COMMENT': {
        
            if (!state[action.postID]) {
                return { ...state, [action.postID]: [action.comment] }
            } else {
                return { ...state, [action.postID]: [...state[action.postID], action.comment] }
            }

        }
        case 'LOAD_COMMENTS': {
            return action.comments
        }
        default: {
            return state;
        }

    }
}

const posts = (state = POSTS, action) => {

    switch (action.type) {

        case 'LOAD_POSTS': {
            return action.posts;
        }

        case 'REMOVE_PHOTO': {
            const newState = state.filter(post => post.id !== action.id);
            return newState;
        };

        case 'ADD_PHOTO': {
            return [...state, action.photo]
        }

        default: {
            return state;
        }
    }
}

const rootReducer = combineReducers({ posts, comments })

export default rootReducer;