import * as firebase from 'firebase';

// Your web app's Firebase configuration
export const firebaseConfig = {
    apiKey: "AIzaSyBSA3dz66QzaGP_LY9wrsV2EDkW8QLdW50",
    authDomain: "photowall-51b7f.firebaseapp.com",
    databaseURL: "https://photowall-51b7f.firebaseio.com",
    projectId: "photowall-51b7f",
    storageBucket: "photowall-51b7f.appspot.com",
    messagingSenderId: "483854953296",
    appId: "1:483854953296:web:73d265e9fb285d5d369a55"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const database = firebase.database();

export {database}