import React from 'react';
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';

const Photo = (props) => {
    console.log(props);
    const post = props.post;
    return <figure className="figure">
        <Link to={`/single/${post.id}`}>  <img className="photo" src={post.imageLink} alt={post.description} /> </Link>
        <figcaption> <p> {post.description}</p> </figcaption>
        <section className="button-container">
            <button className="button" onClick={
                () => {
                    // props.startRemovingPhoto(post.id);
                    // props.startRemovingComments(post.id);
                    props.startRemovingPhotoAndComments(post.id);
                    props.history.push('/');
                }
            }>Remove</button>

            <Link to={`/single/${post.id}`}  className="button">
                <div className="comment-count">
                    <div className="speech-bubble"></div>
                    { props.comments[post.id] ? props.comments[post.id].length : 0 }
                </div>
            </Link>
        </section>
    </figure>
}

Photo.propTypes = {
    post: PropTypes.object.isRequired
}

export default Photo