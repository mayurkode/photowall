import React from 'react';

const Comments = (props) => {

    const submitHandler = (event) => {
        event.preventDefault();
        const comment = event.target.elements.comment.value;
        props.startAddingComment(comment, props.id);
        event.target.reset();
    }

    return <div className="comment">

        <form className="comment-form" onSubmit={submitHandler}>
            <input type="text" placeholder="comment" name="comment" />
            <input type="submit" hidden />
        </form>
        {

            props.comments.reverse().map((comment, index) => {
                return (
                    <p key={index}> {comment} </p>
                )
            })
        }

    </div>
}

export default Comments;