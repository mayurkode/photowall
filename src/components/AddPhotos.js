import React from 'react';
import { Link } from 'react-router-dom';
import Title from './Title';
import PropTypes from 'prop-types';

const AddPhotos = (props) => {

    const submitHandler = (event) => {
        event.preventDefault();
        const link = event.target.elements.link.value;
        const desc = event.target.elements.desc.value;
        const photo = {
            id: Number(new Date()),
            imageLink: link,
            description: desc
        }

        if (link && desc) {
            props.startAddingPost(photo);
            props.history.push('/');
        }
    }

    return <section>
        <Title text="Add Photo" />
        <section className="form">
            <form onSubmit={submitHandler}>
                <input type="text" placeholder="Link" name="link" />
                <input type="text" placeholder="description" name="desc" />
                <button className="button"> Post </button>
            </form> <br></br>

        </section>
        <section className="container">
            <Link className="button" to="/">Go to Timeline </Link>

        </section>
    </section>
}



export default AddPhotos;