import React from 'react';
import Photo from './Photo';
import Title from './Title';
import Comments from './Comments';

const Single = (props) => {

    const { match, posts } = props;
    const id = Number(match.params.id);
    const post = posts.find(post => post.id === id);
    const comments = props.comments[id] || [];

    if (props.loading === true) {
        return <div className="loader">...loading</div>
    }
    else if (post) {
        return <section>
            <Title text='PhotoWall'></Title>
            <div className="single-photo">
                <Photo post={post} {...props} />
                <Comments startAddingComment={props.startAddingComment} comments={comments} id={id} />
            </div>
        </section>
    } else {
        return <h1> No photo found ..</h1>
    }

}

export default Single;