/* eslint-disable no-labels */
import React, { Component } from 'react';
import Title from './Title';
import Photowall from './Photowall';
import AddPhotos from './AddPhotos';
import Single from './Single';
import { Route } from 'react-router-dom';

export default class Main extends Component {

    state = {
        loading: true
    }
    componentDidMount() {
        this.props.startLoadingPosts().then(() => {
            this.setState({ loading: false });
        });
        this.props.startLoadingComments();
    }

    render() {

        return (
            <section>

                <Route exact path="/" render={(params) => (
                    <section>
                        <Title text={'PhotoWall'} />
                        <Photowall {...this.props} {...params} />
                    </section>

                )} />

                <Route exact path="/AddPhoto" render={({ history }) => (
                    <section>
                        <AddPhotos {...this.props} history={history} />
                    </section>

                )} />

                <Route exact path="/single/:id" render={(params) => (
                    <Single loading={this.state.loading} {...this.props} {...params} />
                )} />



            </section>
        )
    }
}