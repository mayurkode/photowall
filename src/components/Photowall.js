import React, { Component } from 'react';
import Photo from './Photo';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default class Photowall extends Component {
    render() {
        return (
            <section>
                <section className="button-container">
                    <Link className="button" to="/AddPhoto"> Add Photo </Link>
                </section>

                <section className="photo-grid">
                    {this.props.posts
                        .sort((x, y) => {
                            return y.id - x.id
                        })
                        .map((post, index) =>
                            <Photo key={post.id} post={post} {...this.props} />
                        )}
                </section>
            </section>

        )
    }
}

Photowall.propTypes = {
    posts: PropTypes.array.isRequired
}