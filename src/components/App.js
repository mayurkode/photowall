import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Main from './Main';
import * as appActions from '../redux/actions';

function mapStateToProps(state) {
    return {
        posts: state.posts,
        comments: state.comments
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(appActions, dispatch);
}

const App = connect(mapStateToProps, mapDispatchToProps)(Main);

export default App;